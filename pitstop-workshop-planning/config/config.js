const fs = require('fs');

module.exports = {
  development: {
    username: 'root',
    password: null,
    database: 'database_development',
    host: 'workshop--db',
    dialect: 'mysql',
    logging: false,
    dialectOptions: {
      bigNumberStrings: true
    }
    },
    test: {
      username: 'root',
      password: null,
      database: 'database_test',
      host: 'workshop--db',
      port: 3306,
      dialect: 'mysql',
      dialectOptions: {
        bigNumberStrings: true
      }
    },
    production: {
      username: 'root',
      password: "password",
      database: 'database_production',
      host: 'workshop--db',
      port: 3306,
      dialect: 'mysql',
      dialectOptions: {
        bigNumberStrings: true,
      }
    }
  };